pytz==2019.2  # https://github.com/stub42/pytz
python-slugify==3.0.3  # https://github.com/un33k/python-slugify
Pillow==6.1.0  # https://github.com/python-pillow/Pillow
argon2-cffi==19.1.0  # https://github.com/hynek/argon2_cffi
redis==3.3.4  # https://github.com/antirez/redis

# Django
# ------------------------------------------------------------------------------
django==2.2.4  # pyup: < 3.0  # https://www.djangoproject.com/
django-environ==0.4.5  # https://github.com/joke2k/django-environ
django-model-utils==3.2.0  # https://github.com/jazzband/django-model-utils
django-allauth==0.39.1  # https://github.com/pennersr/django-allauth
django-crispy-forms==1.7.2  # https://github.com/django-crispy-forms/django-crispy-forms
django-redis==4.10.0  # https://github.com/niwinz/django-redis

# Django REST Framework
djangorestframework==3.10.2  # https://github.com/encode/django-rest-framework
coreapi==2.3.3  # https://github.com/core-api/python-client

django-cors-headers==2.3.0

# Django excel-python
django-import-export==1.1.0

# Django JWT 
djangorestframework-jwt==1.11.0
PyJWT==1.6.4
#documentar apis
django-rest-swagger==2.2.0