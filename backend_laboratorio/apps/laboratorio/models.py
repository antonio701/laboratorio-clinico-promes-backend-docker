from django.db import models

# Create your models here.
from django.db import models
from django.conf import settings
from datetime import datetime 

from django.contrib.auth import get_user_model
User = get_user_model()

#Area de laboratorio
class AreaLaboratorio(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion
    
    def __unicode__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = ("AreaLaboratorio")

#Muestras
class Muestra(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion
    
    def __unicode__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = ("Muestra")

#Examenes compuestos de estudios
class Examen(models.Model):
    descripcion = models.CharField(max_length=100)
    muestra = models.ForeignKey(Muestra, on_delete=models.CASCADE, null=True, blank=True)
    areaLaboratorio = models.ForeignKey(AreaLaboratorio, on_delete=models.CASCADE)
    seleccionado = models.BooleanField(default=False)

    def __str__(self):
        return self.descripcion
    
    def __unicode__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = ("Examenes")

#Estudios de PROMES
class Unidad(models.Model):
    descripcion = models.CharField(max_length=100)

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = ("Unidad")

#Estudios de PROMES
class EstudioGeneralTotal(models.Model):
    TIPO_ESTUDIO = (
        ('1','NUMERICO SIMPLE'),
        ('2','NUMERICO POR GENERO'),
        ('3','TEXTO')
    )
    examen = models.ForeignKey(Examen, on_delete=models.CASCADE, null=True, blank=True)
    descripcion = models.CharField(max_length=100)
    tipoEstudio = models.CharField(max_length=1, choices=TIPO_ESTUDIO)

    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE, null=True, blank=True)
    fvalorini = models.CharField(max_length=100, null=True, blank=True)
    fvalorfin = models.CharField(max_length=100, null=True, blank=True)
    mvalorini = models.CharField(max_length=100, null=True, blank=True)
    mvalorfin = models.CharField(max_length=100, null=True, blank=True)
    nvalorini = models.CharField(max_length=100, null=True, blank=True)
    nvalorfin = models.CharField(max_length=100, null=True, blank=True)
    referencia = models.CharField(max_length=100, null=True, blank=True)
    predeterminado = models.CharField(max_length=500, null=True, blank=True)
    valresaux = models.CharField(max_length=100, null=True, blank=True, default='')


    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion

    class Meta:
        verbose_name_plural = ("EstudioGeneralTotal")


#FLUJO DE ORDENES DE LABORATORIO

#Afiliado
class Afiliado(models.Model):
    SEXO = (
        ('F','FEMENINO'),
        ('M','MASCULINO')
    )
    cedulaIdentidad = models.CharField(max_length=15)
    fechaAfiliacion = models.DateTimeField(default=datetime.now)
    apellidoPaterno = models.CharField(max_length=30)
    apellidoMaterno = models.CharField(max_length=30)
    nombres = models.CharField(max_length=60)
    codigoAfiliacion = models.CharField(max_length=15, primary_key=True)
    sexo = models.CharField(max_length=1, choices=SEXO)
    fechaNacimiento = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.codigoAfiliacion

    def __unicode__(self):
        return self.codigoAfiliacion

    class Meta:
        verbose_name_plural = ("Afiliado")

#Medico
class Medico(models.Model):
    UNIDAD = (
        ('LABORATORIO','LABORATORIO'),
        ('MEDICINA GENERAL','MEDICINA GENERAL'),
        ('IMAGENOLOGIA','IMAGENOLOGIA'),
        ('REGISTRO FICHAJE','REGISTRO FICHAJE'),
    )
    cedulaIdentidad = models.CharField(max_length=15, primary_key=True)
    apellidoPaterno = models.CharField(max_length=30)
    apellidoMaterno = models.CharField(max_length=30)
    nombres = models.CharField(max_length=60)
    especialidad = models.CharField(max_length=40)
    unidad = models.CharField(max_length=30, choices=UNIDAD)
    usuario=models.OneToOneField(User,on_delete=models.CASCADE)

    def __str__(self):
        return self.cedulaIdentidad

    def __unicode__(self):
        return self.cedulaIdentidad

    class Meta:
        verbose_name_plural = ("Medico")

#Orden de laboratorio
class OrdenLaboratorio(models.Model):
    ESTADO = (
        ('V','VIGENTE'),
        ('C','CANCELADO'),
        ('F','FINALIZADO')
    )
    PRECISION = (
        ('EP','EMERGENCIA PROMES'),
        ('ES','EMERGENCIA SSU'),
        ('NP','NORMAL PROMES'),
        ('NS','NORMAL SSU'),
    )
    fechaEmision = models.DateTimeField(default=datetime.now)
    precision = models.CharField(max_length=2, choices=PRECISION)
    estado = models.CharField(max_length=1, choices=ESTADO)
    afiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)
    #examen = models.ForeignKey(Examen, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    #def examenes_solicitados(self):
    #    return ", ".join([p.descripcion for p in self.examen.all()])

    def __unicode__(self):
        return self.fechaEmision

    class Meta:
        verbose_name_plural = ("OrdenLaboratorio")

#Resultados en PDF
class ResultadoPDF(models.Model):
    ordenLaboratorio = models.ForeignKey(OrdenLaboratorio, on_delete=models.CASCADE)
    resultadoPDF = models.FileField(upload_to='resultadosLaboratorio/%Y/%m/%d', null=True, blank=True)
    fechaFinalizacion = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("ResultadoPDF")

#Orden de laboratorio
class DetalleOrden(models.Model):
    ordenLaboratorio = models.ForeignKey(OrdenLaboratorio, on_delete=models.CASCADE)
    examen = models.ForeignKey(Examen, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)
    fechaResultado = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.fechaEmision

    class Meta:
        verbose_name_plural = ("DetalleOrden")

#Resultados en PDF
class Resultado(models.Model):
    TIPO_ESTUDIO = (
        ('1','NUMERICO SIMPLE'),
        ('2','NUMERICO POR GENERO'),
        ('3','TEXTO')
    )
    detalleOrden = models.ForeignKey(DetalleOrden, on_delete=models.CASCADE, null=True, blank=True)
    descripcion = models.CharField(max_length=100, null=True, blank=True)
    tipoEstudio = models.CharField(max_length=1, choices=TIPO_ESTUDIO, null=True, blank=True)

    unidad =  models.CharField(max_length=100, null=True, blank=True)
    fvalorini = models.CharField(max_length=100, null=True, blank=True)
    fvalorfin = models.CharField(max_length=100, null=True, blank=True)
    mvalorini = models.CharField(max_length=100, null=True, blank=True)
    mvalorfin = models.CharField(max_length=100, null=True, blank=True)
    nvalorini = models.CharField(max_length=100, null=True, blank=True)
    nvalorfin = models.CharField(max_length=100, null=True, blank=True)
    referencia = models.CharField(max_length=100, null=True, blank=True)
    predeterminado = models.CharField(max_length=500, null=True, blank=True)
    resultado = models.CharField(max_length=100, null=True, blank=True, default='')

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("Resultado")


#/////////////////////VISTAS DE POSTGRESQL PARA LAS ESTADISTICAS/////////////////////

#CANTIDAD DE PACIENTES FEMENINOS ATENDIDOS POR MES EN DETERMINADA GESTION DE TIPO RUTINA
class PacientesFemeninosRutinaAnual(models.Model):
   
    pacientes_atendidos = models.CharField(max_length=10)
    mes = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'lab_pac_fem_rutina'

#CANTIDAD DE PACIENTES MASCULINOS ATENDIDOS POR MES EN DETERMINADA GESTION DE TIPO RUTINA
class PacientesMasculinosRutinaAnual(models.Model):
   
    pacientes_atendidos = models.CharField(max_length=10)
    mes = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'lab_pac_masc_rutina'

#CANTIDAD DE PACIENTES FEMENINOS ATENDIDOS POR MES EN DETERMINADA GESTION DE TIPO EMERGENCIA
class PacientesFemeninosEmergenciaAnual(models.Model):
   
    pacientes_atendidos = models.CharField(max_length=10)
    mes = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'lab_pac_fem_emergencia'

#CANTIDAD DE PACIENTES MASCULINOS ATENDIDOS POR MES EN DETERMINADA GESTION DE TIPO EMERGENCIA
class PacientesMasculinosEmergenciaAnual(models.Model):
   
    pacientes_atendidos = models.CharField(max_length=10)
    mes = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'lab_pac_masc_emergencia'

#EXAMENES REALIZADOSEN GENERAL (SOLO CANTIDAD)
class ExamenesRealizados(models.Model):

    examen = models.CharField(max_length=100)
    fecha = models.DateTimeField(default=datetime.now)
    area = models.CharField(max_length=100)
    tipo = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'examenes_realizados'

#EXAMENES Y AREAS
class ExamenArea(models.Model):

    id = models.CharField(max_length=100, primary_key=True)
    examen = models.CharField(max_length=100)
    area = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'examen_area'

#EXAMENES REALIZADOS POR PACIENTE (HISTORICO)
class HistoricoPorPaciente(models.Model):

    id_ordenlaboratorio = models.CharField(max_length=100)
    codigoafiliado = models.CharField(max_length=100)
    id_detalleordenlaboratorio = models.CharField(max_length=100)
    id_examen = models.CharField(max_length=100)
    examen = models.CharField(max_length=100)
    tipo = models.CharField(max_length=100)
    area = models.CharField(max_length=100)
    fecha_laboratorio = models.DateTimeField(default=datetime.now)
    medico = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'historico_por_paciente'

#EXAMENES REALIZADOS POR PACIENTE (HISTORICO) FINALIZADOS
class HistoricoPorPacienteFinalizado(models.Model):

    id_orden = models.CharField(max_length=100)
    id_afiliado = models.CharField(max_length=100)
    precision = models.CharField(max_length=100)
    pdf = models.FileField()
    fecha = models.DateTimeField(default=datetime.now)
    class Meta:
        managed = False
        db_table = 'historico_por_paciente_finalizado'

#ORDENES DE LABORATORIO CANCELADAS
class OrdenLaboratorioCancelada(models.Model):

    ordenLaboratorio = models.ForeignKey(OrdenLaboratorio, on_delete=models.CASCADE)
    fechaCancelacion = models.DateTimeField(default=datetime.now)
    motivo = models.CharField(max_length=500)

    def __str__(self):
        return self.ordenLaboratorio

    def __unicode__(self):
        return self.ordenLaboratorio

    class Meta:
        verbose_name_plural = ("OrdenLaboratorioCancelada")





#MODELOS DE IMAGENOLOGIA //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#Orden de gabinete
class OrdenGabinete(models.Model):
    ESTADO = (
        ('V','VIGENTE'),
        ('C','CANCELADO'),
        ('F','FINALIZADO')
    )
    fechaEmision = models.DateTimeField(default=datetime.now)
    estado = models.CharField(max_length=1, choices=ESTADO)
    afiliado = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
    medico = models.ForeignKey(Medico, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("OrdenGabinete")


#Estudio radiologico
class EstudioRadiologico(models.Model):
    ESTADO = (
        ('V','VIGENTE'),
        ('F','FINALIZADO')
    )
    direccion = models.CharField(max_length=500)
    telefono = models.CharField(max_length=50)
    celular = models.CharField(max_length=50)
    email = models.CharField(max_length=500)
    fechaInicioServicio = models.DateTimeField(default=datetime.now)
    fechaFinServicio = models.DateTimeField(default=datetime.now)
    estado = models.CharField(max_length=1, choices=ESTADO)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("EstudioRadiologico")


#Categoria de examen
class CategoriaExamen(models.Model):
    descripcion = models.CharField(max_length=500)
    
    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("CategoriaExamen")

#Examen radiologico
class ExamenRadiologico(models.Model):
    AREA = (
        ('TORAX','TORAX'),
        ('ABDOMEN','ABDOMEN'),
        ('CRANEO Y CARA','CRANEO Y CARA'),
        ('ORTOPANTOMOGRAFIA CEFALOMETRIA','ORTOPANTOMOGRAFIA CEFALOMETRIA'),
        ('COLUMNA','COLUMNA'),
        ('EXTREMIDADES SUPERIORES','EXTREMIDADES SUPERIORES'),
        ('EXTREMIDADES INFERIORES','EXTREMIDADES INFERIORES'),
        ('SERIES OSEAS','SERIES OSEAS'),
        ('RADIOLOGIA SIMPLE PORTATIL','RADIOLOGIA SIMPLE PORTATIL'),
        ('EXPLORACIONES EN QUIROFANO','EXPLORACIONES EN QUIROFANO'),
        ('DENSITOMETRIAS OSEAS','DENSITOMETRIAS OSEAS'),
        ('MAMOGRAFIA','MAMOGRAFIA'),
        ('MAMOGRAFIA CON TOMOSINTESIS','MAMOGRAFIA CON TOMOSINTESIS')
    )
    descripcion = models.CharField(max_length=500)
    categoriaExamen = models.ForeignKey(CategoriaExamen, on_delete=models.CASCADE)
    area = models.CharField(max_length=50, choices=AREA)
    #estudioRadiologico = models.ForeignKey(EstudioRadiologico, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("ExamenRadiologico")

#Detalle de orden de gabinete
class DetalleOrdenGabinete(models.Model):
    ordenGabinete = models.ForeignKey(OrdenGabinete, on_delete=models.CASCADE)
    examenRadiologico = models.ForeignKey(ExamenRadiologico, on_delete=models.CASCADE)
    
    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("DetalleOrdenGabinete")

#Resultados en PDF
class ResultadoGabinetePDF(models.Model):
    ordenGabinete = models.ForeignKey(OrdenGabinete, on_delete=models.CASCADE)
    resultadoPDF = models.FileField(upload_to='resultadosLaboratorio/%Y/%m/%d', null=True, blank=True)
    fechaFinalizacion = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return str(self.id)

    def __unicode__(self):
        return self.id

    class Meta:
        verbose_name_plural = ("ResultadoGabinetePDF")

#Ordenes de gabinete canceldas
class OrdenGabineteCancelada(models.Model):

    ordenGabinete = models.ForeignKey(OrdenGabinete, on_delete=models.CASCADE)
    fechaCancelacion = models.DateTimeField(default=datetime.now)
    motivo = models.CharField(max_length=500)

    def __str__(self):
        return self.ordenGabinete

    def __unicode__(self):
        return self.ordenGabinete

    class Meta:
        verbose_name_plural = ("OrdenGabineteCancelada")
        