from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import AreaLaboratorioLista, AreaLaboratorioDetalle, MuestraLista, MuestraDetalle, ExamenLista, ExamenDetalle, UnidadLista, UnidadDetalle, EstudioGeneralTotalListaPorExamen, EstudioGeneralTotalCrear, EstudioGeneralTotalDetalle, OrdenLaboratorioLista, OrdenLaboratorioDetalle, OrdenLaboratorioVigenteLista, PacientesFemeninosRutinaAnualLista, PacientesMasculinosRutinaAnualLista, PacientesFemeninosEmergenciaAnualLista, PacientesMasculinosEmergenciaAnualLista, ExamenesRealizadosLista, DetalleOrdenLista, DetalleOrdenDetalle, OrdenLaboratorioCanceladaLista, OrdenLaboratorioCanceladaDetalle, AfiliadoLista, AfiliadoDetalle, ExamenListaPorArea, DetalleOrdenCrear, ResultadoPDFLista, ResultadoPDFDetalle, HistoricoPorPacienteLista, ExamenAreaLista, ExamenesDelDiaLista, ResultadoCrear, ResultadoDetalle, ResultadoListaPorExamen, HistoricoPorPacienteFinalizadoLista, OrdenGabineteLista, OrdenGabineteDetalle, DetalleOrdenGabineteLista, DetalleOrdenGabineteCrear, DetalleOrdenGabineteDetalle, OrdenGabineteVigenteLista, ResultadoGabinetePDFLista, ResultadoGabinetePDFDetalle, OrdenGabineteCanceladaLista, MedicoLista, MedicoDetalle

urlpatterns = [
    path('areaLaboratorioLista/', AreaLaboratorioLista.as_view(), name="AreaLaboratorioLista"),
    path('areaLaboratorioDetalle/<int:pk>/', AreaLaboratorioDetalle.as_view(), name="AreaLaboratorioDetalle"),
    path('muestraLista/', MuestraLista.as_view(), name="MuestraLista"),
    path('muestraDetalle/<int:pk>/', MuestraDetalle.as_view(), name="MuestraDetalle"),
    path('examenListaPorArea/<int:area>/', ExamenListaPorArea.as_view(), name="ExamenListaPorArea"),
    path('examenLista/', ExamenLista.as_view(), name="ExamenLista"),
    path('examenDetalle/<int:pk>/', ExamenDetalle.as_view(), name="ExamenDetalle"),
    path('unidadLista/', UnidadLista.as_view(), name="UnidadLista"),
    path('unidadDetalle/<int:pk>/', UnidadDetalle.as_view(), name="UnidadDetalle"),

    path('estudioGeneralTotalListaPorExamen/<int:pk>/', EstudioGeneralTotalListaPorExamen.as_view(), name="EstudioGeneralTotalListaPorExamen"),
    path('estudioGeneralTotalCrear/', EstudioGeneralTotalCrear.as_view(), name="EstudioGeneralTotalCrear"),
    path('estudioGeneralTotalDetalle/<int:pk>/', EstudioGeneralTotalDetalle.as_view(), name="EstudioGeneralTotalDetalle"),

    path('resultadoCrear/', ResultadoCrear.as_view(), name="ResultadoCrear"),
    path('resultadoListaPorExamen/<int:pk>/', ResultadoListaPorExamen.as_view(), name="ResultadoListaPorExamen"),
    path('resultadoDetalle/<int:pk>/', ResultadoDetalle.as_view(), name="ResultadoDetalle"),


    path('ordenLaboratorioLista/', OrdenLaboratorioLista.as_view(), name="OrdenLaboratorioLista"),
    path('ordenLaboratorioDetalle/<int:pk>/', OrdenLaboratorioDetalle.as_view(), name="OrdenLaboratorioDetalle"),
    path('ordenLaboratorioVigenteLista/', OrdenLaboratorioVigenteLista.as_view(), name="OrdenLaboratorioVigenteLista"),
    path('ordenLaboratorioCanceladaLista/', OrdenLaboratorioCanceladaLista.as_view(), name="OrdenLaboratorioCanceladaLista"),

    path('ordenGabineteCanceladaLista/', OrdenGabineteCanceladaLista.as_view(), name="OrdenGabineteCanceladaLista"),

    path('resultadoPDFLista/', ResultadoPDFLista.as_view(), name="ResultadoPDFLista"),
    path('resultadoGabinetePDFLista/', ResultadoGabinetePDFLista.as_view(), name="ResultadoGabinetePDFLista"),


    path('detalleOrdenLista/<int:ordenLaboratorio>/', DetalleOrdenLista.as_view(), name="DetalleOrdenLista"),
    path('detalleOrdenCrear/', DetalleOrdenCrear.as_view(), name="DetalleOrdenCrear"),
    path('detalleOrdenDetalle/<int:pk>/', DetalleOrdenDetalle.as_view(), name="DetalleOrdenDetalle"),

    path('pacientesFemeninosRutinaAnualLista/', PacientesFemeninosRutinaAnualLista.as_view(), name="PacientesFemeninosRutinaAnualLista"),
    path('pacientesMasculinosRutinaAnualLista/', PacientesMasculinosRutinaAnualLista.as_view(), name="PacientesMasculinosRutinaAnualLista"),
    path('pacientesFemeninosEmergenciaAnualLista/', PacientesFemeninosEmergenciaAnualLista.as_view(), name="PacientesFemeninosEmergenciaAnualLista"),
    path('pacientesMasculinosEmergenciaAnualLista/', PacientesMasculinosEmergenciaAnualLista.as_view(), name="PacientesMasculinosEmergenciaAnualLista"),
    path('examenesRealizadosLista/<str:fechaini>/<str:fechafin>/<str:area>/<str:tipo>/', ExamenesRealizadosLista.as_view(), name="ExamenesRealizadosLista"),
    path('examenAreaLista/', ExamenAreaLista.as_view(), name="ExamenAreaLista"),

    path('historicoPorPacienteLista/<str:codigoafiliado>/', HistoricoPorPacienteLista.as_view(), name="HistoricoPorPacienteLista"),
    path('historicoPorPacienteFinalizadoLista/<str:codigoafiliado>/', HistoricoPorPacienteFinalizadoLista.as_view(), name="HistoricoPorPacienteFinalizadoLista"),

    path('examenesDelDiaLista/<str:fechalaboratorio>/', ExamenesDelDiaLista.as_view(), name="ExamenesDelDiaLista"),

    path('afiliadoLista/', AfiliadoLista.as_view(), name="AfiliadoLista"),
    path('afiliadoDetalle/<str:pk>/', AfiliadoDetalle.as_view(), name="AfiliadoDetalle"),

    path('medicoLista/', MedicoLista.as_view(), name="MedicoLista"),
    path('medicoDetalle/<str:usr>/', MedicoDetalle.as_view(), name="MedicoDetalle"),

    path('ordenGabineteLista/', OrdenGabineteLista.as_view(), name="OrdenGabineteLista"),
    path('ordenGabineteDetalle/<int:pk>/', OrdenGabineteDetalle.as_view(), name="OrdenGabineteDetalle"),
    path('ordenGabineteVigenteLista/', OrdenGabineteVigenteLista.as_view(), name="OrdenGabineteVigenteLista"),

    path('detalleOrdenGabineteLista/<int:ordenGabinete>/', DetalleOrdenGabineteLista.as_view(), name="DetalleOrdenGabineteLista"),
    path('detalleOrdenGabineteCrear/', DetalleOrdenGabineteCrear.as_view(), name="DetalleOrdenGabineteCrear"),
    path('detalleOrdenGabineteDetalle/<int:pk>/', DetalleOrdenGabineteDetalle.as_view(), name="DetalleOrdenGabineteDetalle"),

]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'html'])
