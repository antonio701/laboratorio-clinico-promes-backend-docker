from django.contrib import admin

# Register your models here.
from .models import AreaLaboratorio, Muestra, Examen, Unidad, EstudioGeneralTotal, Afiliado, Medico, OrdenLaboratorio, DetalleOrden, OrdenLaboratorioCancelada, ResultadoPDF, Resultado, OrdenGabinete, CategoriaExamen, ExamenRadiologico, DetalleOrdenGabinete, ResultadoGabinetePDF, OrdenGabineteCancelada

class AreaLaboratorioAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion')
admin.site.register(AreaLaboratorio, AreaLaboratorioAdmin)

class MuestraAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion')
admin.site.register(Muestra, MuestraAdmin)

class ExamenAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion', 'muestra', 'areaLaboratorio', 'seleccionado')
admin.site.register(Examen, ExamenAdmin)

class UnidadAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion')
admin.site.register(Unidad, UnidadAdmin)

class EstudioGeneralTotalAdmin(admin.ModelAdmin):
    list_display = ('id', 'examen', 'descripcion', 'tipoEstudio', 'unidad', 'fvalorini', 'fvalorfin', 'mvalorini', 'mvalorfin', 'nvalorini', 'nvalorfin', 'referencia', 'predeterminado', 'valresaux')
admin.site.register(EstudioGeneralTotal,EstudioGeneralTotalAdmin)

class ResultadoAdmin(admin.ModelAdmin):
    list_display = ('id', 'detalleOrden', 'descripcion', 'tipoEstudio', 'unidad', 'fvalorini', 'fvalorfin', 'mvalorini', 'mvalorfin', 'nvalorini', 'nvalorfin', 'referencia', 'predeterminado', 'resultado')
admin.site.register(Resultado,ResultadoAdmin)

class AfiliadoAdmin(admin.ModelAdmin):
    list_display = ('codigoAfiliacion', 'cedulaIdentidad', 'fechaAfiliacion', 'apellidoPaterno', 'apellidoMaterno', 'nombres','fechaNacimiento','sexo')
admin.site.register(Afiliado, AfiliadoAdmin)

class MedicoAdmin(admin.ModelAdmin):
    list_display = ('cedulaIdentidad', 'apellidoPaterno', 'apellidoMaterno', 'nombres', 'especialidad', 'usuario', 'unidad')
admin.site.register(Medico, MedicoAdmin)

class OrdenLaboratorioAdmin(admin.ModelAdmin):
    list_display = ('id','fechaEmision', 'precision', 'estado', 'afiliado', 'medico')
admin.site.register(OrdenLaboratorio, OrdenLaboratorioAdmin)

#class ResultadoPDFAdmin(admin.ModelAdmin):
#    list_display = ('id','ordenLaboratorio', 'resultadoPDF')
#admin.site.register(ResultadoPDF, ResultadoPDFAdmin)

class DetalleOrdenAdmin(admin.ModelAdmin):
    list_display = ('id','ordenLaboratorio', 'examen', 'estado', 'fechaResultado')
admin.site.register(DetalleOrden, DetalleOrdenAdmin)

class OrdenLaboratorioCanceladaAdmin(admin.ModelAdmin):
    list_display = ('id','ordenLaboratorio', 'fechaCancelacion', 'motivo')
admin.site.register(OrdenLaboratorioCancelada, OrdenLaboratorioCanceladaAdmin)

class ResultadoPDFAdmin(admin.ModelAdmin):
    fields = ["resultadoPDF",]
    list_display = ('id','ordenLaboratorio', 'resultadoPDF', 'fechaFinalizacion')
    readonly_fields = ["headshot_image"]
    def headshot_image(self, obj):
     return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
        url = obj.resultadoPDF.url,
        width=obj.resultadoPDF.width,
        height=obj.resultadoPDF.height,
        )
    )
admin.site.register(ResultadoPDF, ResultadoPDFAdmin)


#MODELOS DE IMAGENOLOGIA //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OrdenGabineteAdmin(admin.ModelAdmin):
    list_display = ('id', 'fechaEmision', 'estado', 'afiliado', 'medico')
admin.site.register(OrdenGabinete, OrdenGabineteAdmin)

#class EstudioRadiologicoAdmin(admin.ModelAdmin):
#    list_display = ('id', 'direccion', 'telefono', 'celular', 'email', 'fechaInicioServicio', 'fechaFinServicio', 'estado')
#admin.site.register(EstudioRadiologico, EstudioRadiologicoAdmin)

class CategoriaExamenAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion')
admin.site.register(CategoriaExamen, CategoriaExamenAdmin)

class ExamenRadiologicoAdmin(admin.ModelAdmin):
    list_display = ('id', 'descripcion', 'categoriaExamen', 'area')
admin.site.register(ExamenRadiologico, ExamenRadiologicoAdmin)

class DetalleOrdenGabineteAdmin(admin.ModelAdmin):
    list_display = ('id', 'ordenGabinete', 'examenRadiologico')
admin.site.register(DetalleOrdenGabinete, DetalleOrdenGabineteAdmin)

class ResultadoGabinetePDFAdmin(admin.ModelAdmin):
    fields = ["resultadoPDF",]
    list_display = ('id','ordenGabinete', 'resultadoPDF', 'fechaFinalizacion')
    readonly_fields = ["headshot_image"]
    def headshot_image(self, obj):
     return mark_safe('<img src="{url}" width="{width}" height={height} />'.format(
        url = obj.resultadoPDF.url,
        width=obj.resultadoPDF.width,
        height=obj.resultadoPDF.height,
        )
    )
admin.site.register(ResultadoGabinetePDF, ResultadoGabinetePDFAdmin)

class OrdenGabineteCanceladaAdmin(admin.ModelAdmin):
    list_display = ('id','ordenGabinete', 'fechaCancelacion', 'motivo')
admin.site.register(OrdenGabineteCancelada, OrdenGabineteCanceladaAdmin)
