from rest_framework import serializers
from .models import AreaLaboratorio, Muestra, Examen, Unidad, EstudioGeneralTotal, Afiliado, Medico, OrdenLaboratorio, PacientesFemeninosRutinaAnual, PacientesMasculinosRutinaAnual, PacientesFemeninosEmergenciaAnual, PacientesMasculinosEmergenciaAnual, ExamenesRealizados, DetalleOrden, OrdenLaboratorioCancelada, ResultadoPDF, HistoricoPorPaciente, ExamenArea, Resultado, HistoricoPorPacienteFinalizado, OrdenGabinete, DetalleOrdenGabinete, CategoriaExamen, ExamenRadiologico, ResultadoGabinetePDF, OrdenGabineteCancelada
from ..usuarios_login.serializers import UserSerializer
import six
import base64
import uuid
import imghdr
from django.core.files.base import ContentFile

class AreaLaboratorioSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaLaboratorio
        fields = '__all__'

class MuestraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Muestra
        fields = '__all__'

class ExamenSerializer(serializers.ModelSerializer):
    muestra = MuestraSerializer(read_only=True)
    areaLaboratorio = AreaLaboratorioSerializer(read_only=True)
    class Meta:
        model = Examen
        fields = '__all__'

class ExamenWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Examen
        fields = '__all__'

class UnidadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unidad
        fields = '__all__'

class EstudioGeneralTotalSerializer(serializers.ModelSerializer):
    unidad = UnidadSerializer(read_only=True)
    class Meta:
        model = EstudioGeneralTotal
        fields = '__all__'

class EstudioGeneralTotalWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstudioGeneralTotal
        fields = '__all__'

class ResultadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resultado
        fields = '__all__'

class ResultadoWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resultado
        fields = '__all__'

class AfiliadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Afiliado
        fields = '__all__'

class MedicoSerializer(serializers.ModelSerializer):
    usuario=UserSerializer(read_only=True)
    class Meta:
        model = Medico
        fields = '__all__'
    
class OrdenLaboratorioSerializer(serializers.ModelSerializer):
    afiliado = AfiliadoSerializer(read_only=True)
    medico = MedicoSerializer(read_only=True)
    class Meta:
        model = OrdenLaboratorio
        fields = '__all__'

class OrdenLaboratorioWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenLaboratorio
        fields = '__all__'

class ResultadoPDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResultadoPDF
        fields = ['ordenLaboratorio', 'resultadoPDF']

class DetalleOrdenSerializer(serializers.ModelSerializer):
    ordenLaboratorio = OrdenLaboratorioSerializer(read_only=True)
    examen = ExamenSerializer(read_only=True)
    class Meta:
        model = DetalleOrden
        fields = '__all__'

class DetalleOrdenWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetalleOrden
        fields = '__all__'

class PacientesFemeninosRutinaAnualSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacientesFemeninosRutinaAnual
        fields = '__all__'

class PacientesMasculinosRutinaAnualSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacientesMasculinosRutinaAnual
        fields = '__all__'
        
class PacientesFemeninosEmergenciaAnualSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacientesFemeninosEmergenciaAnual
        fields = '__all__'

class PacientesMasculinosEmergenciaAnualSerializer(serializers.ModelSerializer):
    class Meta:
        model = PacientesMasculinosEmergenciaAnual
        fields = '__all__'

class ExamenesRealizadosSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExamenesRealizados
        fields = '__all__'

class ExamenAreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExamenArea
        fields = '__all__'

class HistoricoPorPacienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoricoPorPaciente
        fields = '__all__'

class HistoricoPorPacienteFinalizadoSerializer(serializers.ModelSerializer):
    class Meta:
        model = HistoricoPorPacienteFinalizado
        fields = ['id_orden', 'id_afiliado', 'precision', 'pdf', 'fecha']

class OrdenLaboratorioCanceladaWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenLaboratorioCancelada
        fields = '__all__'


class OrdenLaboratorioCanceladaSerializer(serializers.ModelSerializer):
    ordenLaboratorio = OrdenLaboratorioSerializer(read_only=True)
    class Meta:
        model =OrdenLaboratorioCancelada
        fields = '__all__'


#MODELOS DE IMAGENOLOGIA //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OrdenGabineteSerializer(serializers.ModelSerializer):
    afiliado = AfiliadoSerializer(read_only=True)
    medico = MedicoSerializer(read_only=True)
    class Meta:
        model = OrdenGabinete
        fields = '__all__'

class OrdenGabineteWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenGabinete
        fields = '__all__'

class CategoriaExamenSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoriaExamen
        fields = '__all__'

class ExamenRadiologicoSerializer(serializers.ModelSerializer):
    categoriaExamen = CategoriaExamenSerializer(read_only=True)
    class Meta:
        model = ExamenRadiologico
        fields = '__all__'

class DetalleOrdenGabineteSerializer(serializers.ModelSerializer):
    ordenGabinete = OrdenGabineteSerializer(read_only=True)
    examenRadiologico = ExamenRadiologicoSerializer(read_only=True)
    class Meta:
        model = DetalleOrdenGabinete
        fields = '__all__'

class DetalleOrdenGabineteWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetalleOrdenGabinete
        fields = '__all__'

class ResultadoGabinetePDFSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResultadoGabinetePDF
        fields = ['ordenGabinete', 'resultadoPDF']

class OrdenGabineteCanceladaWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrdenGabineteCancelada
        fields = '__all__'


class OrdenGabineteCanceladaSerializer(serializers.ModelSerializer):
    ordenGabinete = OrdenGabineteSerializer(read_only=True)
    class Meta:
        model =OrdenGabineteCancelada
        fields = '__all__'
        