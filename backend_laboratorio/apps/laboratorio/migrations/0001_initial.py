# Generated by Django 2.2.4 on 2019-08-01 21:01

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ExamenArea',
            fields=[
                ('id', models.CharField(max_length=100, primary_key=True, serialize=False)),
                ('examen', models.CharField(max_length=100)),
                ('area', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'examen_area',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='ExamenesRealizados',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('examen', models.CharField(max_length=100)),
                ('fecha', models.DateTimeField(default=datetime.datetime.now)),
                ('area', models.CharField(max_length=100)),
                ('tipo', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'examenes_realizados',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HistoricoPorPaciente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_ordenlaboratorio', models.CharField(max_length=100)),
                ('codigoafiliado', models.CharField(max_length=100)),
                ('id_detalleordenlaboratorio', models.CharField(max_length=100)),
                ('id_examen', models.CharField(max_length=100)),
                ('examen', models.CharField(max_length=100)),
                ('tipo', models.CharField(max_length=100)),
                ('area', models.CharField(max_length=100)),
                ('fecha_laboratorio', models.DateTimeField(default=datetime.datetime.now)),
                ('medico', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'historico_por_paciente',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='HistoricoPorPacienteFinalizado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_orden', models.CharField(max_length=100)),
                ('id_afiliado', models.CharField(max_length=100)),
                ('precision', models.CharField(max_length=100)),
                ('pdf', models.FileField(upload_to='')),
                ('fecha', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'db_table': 'historico_por_paciente_finalizado',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PacientesFemeninosEmergenciaAnual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pacientes_atendidos', models.CharField(max_length=10)),
                ('mes', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lab_pac_fem_emergencia',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PacientesFemeninosRutinaAnual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pacientes_atendidos', models.CharField(max_length=10)),
                ('mes', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lab_pac_fem_rutina',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PacientesMasculinosEmergenciaAnual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pacientes_atendidos', models.CharField(max_length=10)),
                ('mes', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lab_pac_masc_emergencia',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PacientesMasculinosRutinaAnual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pacientes_atendidos', models.CharField(max_length=10)),
                ('mes', models.CharField(max_length=20)),
            ],
            options={
                'db_table': 'lab_pac_masc_rutina',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Afiliado',
            fields=[
                ('cedulaIdentidad', models.CharField(max_length=15)),
                ('fechaAfiliacion', models.DateTimeField(default=datetime.datetime.now)),
                ('apellidoPaterno', models.CharField(max_length=30)),
                ('apellidoMaterno', models.CharField(max_length=30)),
                ('nombres', models.CharField(max_length=60)),
                ('codigoAfiliacion', models.CharField(max_length=15, primary_key=True, serialize=False)),
                ('sexo', models.CharField(choices=[('F', 'FEMENINO'), ('M', 'MASCULINO')], max_length=1)),
                ('fechaNacimiento', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'verbose_name_plural': 'Afiliado',
            },
        ),
        migrations.CreateModel(
            name='AreaLaboratorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'AreaLaboratorio',
            },
        ),
        migrations.CreateModel(
            name='CategoriaExamen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=500)),
            ],
            options={
                'verbose_name_plural': 'CategoriaExamen',
            },
        ),
        migrations.CreateModel(
            name='DetalleOrden',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.BooleanField(default=True)),
                ('fechaResultado', models.DateTimeField(default=datetime.datetime.now)),
            ],
            options={
                'verbose_name_plural': 'DetalleOrden',
            },
        ),
        migrations.CreateModel(
            name='EstudioRadiologico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('direccion', models.CharField(max_length=500)),
                ('telefono', models.CharField(max_length=50)),
                ('celular', models.CharField(max_length=50)),
                ('email', models.CharField(max_length=500)),
                ('fechaInicioServicio', models.DateTimeField(default=datetime.datetime.now)),
                ('fechaFinServicio', models.DateTimeField(default=datetime.datetime.now)),
                ('estado', models.CharField(choices=[('V', 'VIGENTE'), ('F', 'FINALIZADO')], max_length=1)),
            ],
            options={
                'verbose_name_plural': 'EstudioRadiologico',
            },
        ),
        migrations.CreateModel(
            name='Medico',
            fields=[
                ('cedulaIdentidad', models.CharField(max_length=15, primary_key=True, serialize=False)),
                ('apellidoPaterno', models.CharField(max_length=30)),
                ('apellidoMaterno', models.CharField(max_length=30)),
                ('nombres', models.CharField(max_length=60)),
                ('especialidad', models.CharField(max_length=40)),
                ('unidad', models.CharField(choices=[('LABORATORIO', 'LABORATORIO'), ('MEDICINA GENERAL', 'MEDICINA GENERAL'), ('IMAGENOLOGIA', 'IMAGENOLOGIA'), ('REGISTRO FICHAJE', 'REGISTRO FICHAJE')], max_length=30)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Medico',
            },
        ),
        migrations.CreateModel(
            name='Muestra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Muestra',
            },
        ),
        migrations.CreateModel(
            name='OrdenGabinete',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fechaEmision', models.DateTimeField(default=datetime.datetime.now)),
                ('estado', models.CharField(choices=[('V', 'VIGENTE'), ('C', 'CANCELADO'), ('F', 'FINALIZADO')], max_length=1)),
                ('afiliado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Afiliado')),
                ('medico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Medico')),
            ],
            options={
                'verbose_name_plural': 'OrdenGabinete',
            },
        ),
        migrations.CreateModel(
            name='OrdenLaboratorio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fechaEmision', models.DateTimeField(default=datetime.datetime.now)),
                ('precision', models.CharField(choices=[('EP', 'EMERGENCIA PROMES'), ('ES', 'EMERGENCIA SSU'), ('NP', 'NORMAL PROMES'), ('NS', 'NORMAL SSU')], max_length=2)),
                ('estado', models.CharField(choices=[('V', 'VIGENTE'), ('C', 'CANCELADO'), ('F', 'FINALIZADO')], max_length=1)),
                ('afiliado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Afiliado')),
                ('medico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Medico')),
            ],
            options={
                'verbose_name_plural': 'OrdenLaboratorio',
            },
        ),
        migrations.CreateModel(
            name='Unidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'Unidad',
            },
        ),
        migrations.CreateModel(
            name='ResultadoPDF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resultadoPDF', models.FileField(blank=True, null=True, upload_to='resultadosLaboratorio/%Y/%m/%d')),
                ('fechaFinalizacion', models.DateTimeField(default=datetime.datetime.now)),
                ('ordenLaboratorio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenLaboratorio')),
            ],
            options={
                'verbose_name_plural': 'ResultadoPDF',
            },
        ),
        migrations.CreateModel(
            name='ResultadoGabinetePDF',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('resultadoPDF', models.FileField(blank=True, null=True, upload_to='resultadosLaboratorio/%Y/%m/%d')),
                ('fechaFinalizacion', models.DateTimeField(default=datetime.datetime.now)),
                ('ordenGabinete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenGabinete')),
            ],
            options={
                'verbose_name_plural': 'ResultadoGabinetePDF',
            },
        ),
        migrations.CreateModel(
            name='Resultado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(blank=True, max_length=100, null=True)),
                ('tipoEstudio', models.CharField(blank=True, choices=[('1', 'NUMERICO SIMPLE'), ('2', 'NUMERICO POR GENERO'), ('3', 'TEXTO')], max_length=1, null=True)),
                ('unidad', models.CharField(blank=True, max_length=100, null=True)),
                ('fvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('fvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('mvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('mvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('nvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('nvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('referencia', models.CharField(blank=True, max_length=100, null=True)),
                ('predeterminado', models.CharField(blank=True, max_length=500, null=True)),
                ('resultado', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('detalleOrden', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='laboratorio.DetalleOrden')),
            ],
            options={
                'verbose_name_plural': 'Resultado',
            },
        ),
        migrations.CreateModel(
            name='OrdenLaboratorioCancelada',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fechaCancelacion', models.DateTimeField(default=datetime.datetime.now)),
                ('motivo', models.CharField(max_length=500)),
                ('ordenLaboratorio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenLaboratorio')),
            ],
            options={
                'verbose_name_plural': 'OrdenLaboratorioCancelada',
            },
        ),
        migrations.CreateModel(
            name='OrdenGabineteCancelada',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fechaCancelacion', models.DateTimeField(default=datetime.datetime.now)),
                ('motivo', models.CharField(max_length=500)),
                ('ordenGabinete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenGabinete')),
            ],
            options={
                'verbose_name_plural': 'OrdenGabineteCancelada',
            },
        ),
        migrations.CreateModel(
            name='ExamenRadiologico',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=500)),
                ('area', models.CharField(choices=[('TORAX', 'TORAX'), ('ABDOMEN', 'ABDOMEN'), ('CRANEO Y CARA', 'CRANEO Y CARA'), ('ORTOPANTOMOGRAFIA CEFALOMETRIA', 'ORTOPANTOMOGRAFIA CEFALOMETRIA'), ('COLUMNA', 'COLUMNA'), ('EXTREMIDADES SUPERIORES', 'EXTREMIDADES SUPERIORES'), ('EXTREMIDADES INFERIORES', 'EXTREMIDADES INFERIORES'), ('SERIES OSEAS', 'SERIES OSEAS'), ('RADIOLOGIA SIMPLE PORTATIL', 'RADIOLOGIA SIMPLE PORTATIL'), ('EXPLORACIONES EN QUIROFANO', 'EXPLORACIONES EN QUIROFANO'), ('DENSITOMETRIAS OSEAS', 'DENSITOMETRIAS OSEAS'), ('MAMOGRAFIA', 'MAMOGRAFIA'), ('MAMOGRAFIA CON TOMOSINTESIS', 'MAMOGRAFIA CON TOMOSINTESIS')], max_length=50)),
                ('categoriaExamen', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.CategoriaExamen')),
            ],
            options={
                'verbose_name_plural': 'ExamenRadiologico',
            },
        ),
        migrations.CreateModel(
            name='Examen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('seleccionado', models.BooleanField(default=False)),
                ('areaLaboratorio', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.AreaLaboratorio')),
                ('muestra', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Muestra')),
            ],
            options={
                'verbose_name_plural': 'Examenes',
            },
        ),
        migrations.CreateModel(
            name='EstudioGeneralTotal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('descripcion', models.CharField(max_length=100)),
                ('tipoEstudio', models.CharField(choices=[('1', 'NUMERICO SIMPLE'), ('2', 'NUMERICO POR GENERO'), ('3', 'TEXTO')], max_length=1)),
                ('fvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('fvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('mvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('mvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('nvalorini', models.CharField(blank=True, max_length=100, null=True)),
                ('nvalorfin', models.CharField(blank=True, max_length=100, null=True)),
                ('referencia', models.CharField(blank=True, max_length=100, null=True)),
                ('predeterminado', models.CharField(blank=True, max_length=500, null=True)),
                ('valresaux', models.CharField(blank=True, default='', max_length=100, null=True)),
                ('examen', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Examen')),
                ('unidad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Unidad')),
            ],
            options={
                'verbose_name_plural': 'EstudioGeneralTotal',
            },
        ),
        migrations.CreateModel(
            name='DetalleOrdenGabinete',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('examenRadiologico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.ExamenRadiologico')),
                ('ordenGabinete', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenGabinete')),
            ],
            options={
                'verbose_name_plural': 'DetalleOrdenGabinete',
            },
        ),
        migrations.AddField(
            model_name='detalleorden',
            name='examen',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.Examen'),
        ),
        migrations.AddField(
            model_name='detalleorden',
            name='ordenLaboratorio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='laboratorio.OrdenLaboratorio'),
        ),
    ]
