from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import status
from .models import AreaLaboratorio, Muestra, Examen, Unidad, EstudioGeneralTotal, Afiliado, Medico, OrdenLaboratorio, PacientesFemeninosRutinaAnual, PacientesMasculinosRutinaAnual, PacientesFemeninosEmergenciaAnual, PacientesMasculinosEmergenciaAnual, ExamenesRealizados, DetalleOrden, OrdenLaboratorioCancelada, ResultadoPDF, HistoricoPorPaciente, ExamenArea, Resultado, HistoricoPorPacienteFinalizado, OrdenGabinete, DetalleOrdenGabinete, ResultadoGabinetePDF, OrdenGabineteCancelada
from .serializers import AreaLaboratorioSerializer, MuestraSerializer, ExamenSerializer, ExamenWriteSerializer, UnidadSerializer, EstudioGeneralTotalSerializer, AfiliadoSerializer, MedicoSerializer, OrdenLaboratorioSerializer, EstudioGeneralTotalWriteSerializer, PacientesFemeninosRutinaAnualSerializer, PacientesMasculinosRutinaAnualSerializer, PacientesFemeninosEmergenciaAnualSerializer, PacientesMasculinosEmergenciaAnualSerializer, ExamenesRealizadosSerializer, DetalleOrdenSerializer, OrdenLaboratorioCanceladaSerializer, OrdenLaboratorioCanceladaWriteSerializer, OrdenLaboratorioWriteSerializer, DetalleOrdenWriteSerializer, ResultadoPDFSerializer, HistoricoPorPacienteSerializer, ExamenAreaSerializer, ResultadoPDFSerializer, ResultadoWriteSerializer, ResultadoSerializer, HistoricoPorPacienteFinalizadoSerializer, OrdenGabineteSerializer, OrdenGabineteWriteSerializer, DetalleOrdenGabineteSerializer, DetalleOrdenGabineteWriteSerializer, ResultadoGabinetePDFSerializer, OrdenGabineteCanceladaSerializer, OrdenGabineteCanceladaWriteSerializer
import os
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

class AreaLaboratorioLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = AreaLaboratorio.objects.all()
        serializer = AreaLaboratorioSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = AreaLaboratorioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AreaLaboratorioDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return AreaLaboratorio.objects.get(pk=pk)
        except AreaLaboratorio.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AreaLaboratorioSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AreaLaboratorioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class MuestraLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = Muestra.objects.all()
        serializer = MuestraSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = MuestraSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MuestraDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Muestra.objects.get(pk=pk)
        except Muestra.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MuestraSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MuestraSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class ExamenListaPorArea(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, area, format=None):
        snippets = Examen.objects.filter(areaLaboratorio=area).order_by('descripcion')
        serializer = ExamenSerializer(snippets, many=True)
        return Response(serializer.data)

class ExamenLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = Examen.objects.order_by('descripcion')
        serializer = ExamenSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ExamenWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ExamenDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Examen.objects.get(pk=pk)
        except Examen.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ExamenSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ExamenSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class UnidadLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = Unidad.objects.all()
        serializer = UnidadSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = UnidadSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UnidadDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Unidad.objects.get(pk=pk)
        except Unidad.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = UnidadSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = UnidadSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class EstudioGeneralTotalListaPorExamen(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, pk, format=None):
        snippets = EstudioGeneralTotal.objects.filter(examen=pk).order_by('id')
        serializer = EstudioGeneralTotalSerializer(snippets, many=True)
        return Response(serializer.data)

class EstudioGeneralTotalCrear(APIView):
    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = EstudioGeneralTotalWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EstudioGeneralTotalDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return EstudioGeneralTotal.objects.get(pk=pk)
        except EstudioGeneralTotal.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EstudioGeneralTotalSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = EstudioGeneralTotalSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class ResultadoListaPorExamen(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, pk, format=None):
        snippets = Resultado.objects.filter(detalleOrden=pk)
        serializer = ResultadoSerializer(snippets, many=True)
        return Response(serializer.data)

class ResultadoCrear(APIView):
    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ResultadoWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ResultadoDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Resultado.objects.get(pk=pk)
        except Resultado.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class AfiliadoLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = Afiliado.objects.all()
        serializer = AfiliadoSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = AfiliadoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AfiliadoDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Afiliado.objects.get(pk=pk)
        except Afiliado.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AfiliadoSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = AfiliadoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class MedicoLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = Medico.objects.all()
        serializer = MedicoSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = MedicoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MedicoDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return Medico.objects.get(pk=pk)
        except Medico.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    #def get(self, request, pk, format=None):
    #    snippet = self.get_object(pk)
    #    serializer = MedicoSerializer(snippet)
    #    return Response(serializer.data)

    def get(self, request, usr, format=None):
        Formulario = Medico.objects.all().filter(usuario__username=usr)
        serializer = MedicoSerializer(Formulario, many=True)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = MedicoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class OrdenLaboratorioLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = OrdenLaboratorio.objects.all()
        serializer = OrdenLaboratorioSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = OrdenLaboratorioWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenLaboratorioDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return OrdenLaboratorio.objects.get(pk=pk)
        except OrdenLaboratorio.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenLaboratorioSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenLaboratorioSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class ResultadoPDFLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = ResultadoPDF.objects.all()
        serializer = ResultadoPDFSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ResultadoPDFSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ResultadoPDFDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return ResultadoPDF.objects.get(pk=pk)
        except ResultadoPDF.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoPDFSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoPDFSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class DetalleOrdenLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, ordenLaboratorio, format=None):
        snippets = DetalleOrden.objects.filter(ordenLaboratorio=ordenLaboratorio).order_by('estado')
        #snippets = DetalleOrden.objects.filter(ordenLaboratorio=ordenLaboratorio, estado=True)
        serializer = DetalleOrdenSerializer(snippets, many=True)
        return Response(serializer.data)

class DetalleOrdenCrear(APIView):
    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = DetalleOrdenWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetalleOrdenDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return DetalleOrden.objects.get(pk=pk)
        except DetalleOrden.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DetalleOrdenSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DetalleOrdenSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class OrdenLaboratorioVigenteLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        #snippets = OrdenLaboratorio.objects.filter(estado='V', precision='EP')
        snippets = OrdenLaboratorio.objects.filter(estado='V').order_by('precision','fechaEmision')
        serializer = OrdenLaboratorioSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = OrdenLaboratorioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#------------------------------------------------------------------------------

class PacientesFemeninosRutinaAnualLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = PacientesFemeninosRutinaAnual.objects.all()
        serializer = PacientesFemeninosRutinaAnualSerializer(snippets, many=True)
        return Response(serializer.data)
    
class PacientesMasculinosRutinaAnualLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = PacientesMasculinosRutinaAnual.objects.all()
        serializer = PacientesMasculinosRutinaAnualSerializer(snippets, many=True)
        return Response(serializer.data)

class PacientesFemeninosEmergenciaAnualLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = PacientesFemeninosEmergenciaAnual.objects.all()
        serializer = PacientesFemeninosEmergenciaAnualSerializer(snippets, many=True)
        return Response(serializer.data)

class PacientesMasculinosEmergenciaAnualLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = PacientesMasculinosEmergenciaAnual.objects.all()
        serializer = PacientesMasculinosEmergenciaAnualSerializer(snippets, many=True)
        return Response(serializer.data)

class ExamenesRealizadosLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, fechaini, fechafin, area, tipo, format=None):
        snippets = ExamenesRealizados.objects.filter(fecha__gte=fechaini, fecha__lte=fechafin, area=area, tipo=tipo)
        serializer = ExamenesRealizadosSerializer(snippets, many=True)
        return Response(serializer.data)

class ExamenAreaLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = ExamenArea.objects.all()
        serializer = ExamenAreaSerializer(snippets, many=True)
        return Response(serializer.data)

class HistoricoPorPacienteLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, codigoafiliado, format=None):
        snippets = HistoricoPorPaciente.objects.filter(codigoafiliado=codigoafiliado).order_by('-id_ordenlaboratorio')
        serializer = HistoricoPorPacienteSerializer(snippets, many=True)
        return Response(serializer.data)

class HistoricoPorPacienteFinalizadoLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, codigoafiliado, format=None):
        snippets = HistoricoPorPacienteFinalizado.objects.filter(id_afiliado=codigoafiliado)
        serializer = HistoricoPorPacienteFinalizadoSerializer(snippets, many=True)
        return Response(serializer.data)

class ExamenesDelDiaLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, fechalaboratorio, format=None):
        snippets = HistoricoPorPaciente.objects.filter(fecha_laboratorio__date=fechalaboratorio)
        serializer = HistoricoPorPacienteSerializer(snippets, many=True)
        return Response(serializer.data)
        
#------------------------------------------------------------------------------

class OrdenLaboratorioCanceladaLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = OrdenLaboratorioCancelada.objects.all()
        serializer = OrdenLaboratorioCanceladaSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = OrdenLaboratorioCanceladaWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenLaboratorioCanceladaDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return OrdenLaboratorioCancelada.objects.get(pk=pk)
        except OrdenLaboratorioCancelada.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenLaboratorioCanceladaSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenLaboratorioCanceladaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




#MODELOS DE IMAGENOLOGIA //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#------------------------------------------------------------------------------

class OrdenGabineteLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = OrdenGabinete.objects.all()
        serializer = OrdenGabineteSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = OrdenGabineteWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenGabineteDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return OrdenGabinete.objects.get(pk=pk)
        except OrdenGabinete.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenGabineteSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenGabineteSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class OrdenGabineteVigenteLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = OrdenGabinete.objects.filter(estado='V').order_by('fechaEmision')
        serializer = OrdenGabineteSerializer(snippets, many=True)
        return Response(serializer.data)


#------------------------------------------------------------------------------

class DetalleOrdenGabineteLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, ordenGabinete, format=None):
        snippets = DetalleOrdenGabinete.objects.filter(ordenGabinete=ordenGabinete)
        #snippets = DetalleOrdenGabinete.objects.filter(ordenLaboratorio=ordenLaboratorio, estado=True)
        serializer = DetalleOrdenGabineteSerializer(snippets, many=True)
        return Response(serializer.data)

class DetalleOrdenGabineteCrear(APIView):
    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = DetalleOrdenGabineteWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetalleOrdenGabineteDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return DetalleOrdenGabinete.objects.get(pk=pk)
        except DetalleOrdenGabinete.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DetalleOrdenGabineteSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = DetalleOrdenGabineteSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class ResultadoGabinetePDFLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = ResultadoGabinetePDF.objects.all()
        serializer = ResultadoGabinetePDFSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = ResultadoGabinetePDFSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ResultadoGabinetePDFDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return ResultadoGabinetePDF.objects.get(pk=pk)
        except ResultadoGabinetePDF.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoGabinetePDFSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ResultadoGabinetePDFSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

#------------------------------------------------------------------------------

class OrdenGabineteCanceladaLista(APIView):
   
    #LISTA TODOS LOS REGISTROS
    def get(self, request, format=None):
        snippets = OrdenGabineteCancelada.objects.all()
        serializer = OrdenGabineteCanceladaSerializer(snippets, many=True)
        return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
    def post(self, request, format=None):
        serializer = OrdenGabineteCanceladaWriteSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrdenGabineteCanceladaDetalle(APIView):
    
    def get_object(self, pk):
        try:
            return OrdenGabineteCancelada.objects.get(pk=pk)
        except OrdenGabineteCancelada.DoesNotExist:
            raise Http404

    #DEVUELVE UN REGISTRO
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenGabineteCanceladaSerializer(snippet)
        return Response(serializer.data)

    #ACTUALIZA UN REGISTRO
    def put(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = OrdenGabineteCanceladaSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    #BORRA UN REGISTRO
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

